package com.hov.remizmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hepha on 1.10.2016.
 */
public class AddedItemAdapter extends ArrayAdapter<ItemCounter> {

    private onRemoveInteraction mListener;

    private int currLang = 0;


    public AddedItemAdapter(Context context, int resource, List<ItemCounter> objects) {
        super(context, resource, objects);
        mListener = (onRemoveInteraction) context;
    }

    public AddedItemAdapter(Context context, int resource) {
        super(context, resource);
        mListener = (onRemoveInteraction) context;
    }

    public void setlang(int i) {
        currLang = i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.adapter_added_item, null);
        }

        TextView tvName = (TextView) v.findViewById(R.id.tvName);
        TextView tvPrice = (TextView) v.findViewById(R.id.tvPrice);
        Button btnRemove = (Button) v.findViewById(R.id.btnRemove);

        ItemCounter i = getItem(position);

        if (tvName != null) {
            if(i.getCounter() > 1)
                tvName.setText(i.getName(currLang) + "(X"+i.getCounter()+")");
            else
                tvName.setText(i.getName(currLang));

            tvName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.openItem(position);
                }
            });
        }
        if (tvPrice != null) {
            tvPrice.setText(String.format("%.2f", (i.getPrice()*i.getCounter())));
        }
        if (btnRemove != null) {
            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.removeItem(position);
                }

            });
            btnRemove.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.clearItem(position);
                    return false;
                }
            });
        }

        return v;
    }

    public interface onRemoveInteraction {
        void removeItem(int position);
        void clearItem(int position);
        void openItem(int position);
    }
}
