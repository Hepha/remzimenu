package com.hov.remizmenu;


import java.util.List;

public class Category {

    private String[] name;
    private int bitmapId;
    List<Item> items;

    public Category(String[] name, int bitmapId, List<Item> items) {
        this.name = name;
        this.bitmapId = bitmapId;
        this.items = items;
    }

    public Category() {
    }

    public String getName(int i) {
        return name[i];
    }

    public void setName(String[] name) {
        this.name = name;
    }

    public int getBitmapId() {
        return bitmapId;
    }

    public void setBitmapId(int bitmapId) {
        this.bitmapId = bitmapId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}