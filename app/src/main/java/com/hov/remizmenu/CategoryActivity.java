package com.hov.remizmenu;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {


    //Views
    LinearLayout ibSoup, ibKids, ibMids, ibZeytin, ibDurum, ibKebap, ibIzgara, ibPide, ibTatli, ibSalata, ibDrinks, ibMangal;
    TextView tvSoup, tvKids, tvMids, tvZeytin, tvDurum, tvKebap, tvIzgara, tvPide, tvTatli, tvSalata, tvDrinks, tvMangal;
    List<LinearLayout> ibArray;
    List<TextView> tvArray;

    LinearLayout llLang;

    ImageView ivFlag;

    Button btnLang;

    TextView tvLang;

    int currLang = 0;

    boolean firstTime = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        if(getIntent().getExtras()!=null){
            firstTime = false;
            currLang = getIntent().getExtras().getInt("lang");
        }

        //TextViews
        tvLang = (TextView)findViewById(R.id.tvLang);

        tvSoup = (TextView) findViewById(R.id.tvSoup);
        tvKids = (TextView) findViewById(R.id.tvKids);
        tvMids = (TextView) findViewById(R.id.tvMids);
        tvZeytin = (TextView) findViewById(R.id.tvZeytins);
        tvDurum = (TextView) findViewById(R.id.tvDurums);
        tvKebap = (TextView) findViewById(R.id.tvKebabs);
        tvIzgara = (TextView) findViewById(R.id.tvIzgara);
        tvPide = (TextView) findViewById(R.id.tvPide);
        tvTatli = (TextView) findViewById(R.id.tvDessert);
        tvSalata = (TextView) findViewById(R.id.tvSalads);
        tvDrinks = (TextView) findViewById(R.id.tvDrinks);
        tvMangal = (TextView) findViewById(R.id.tvMangal);

        tvArray = new ArrayList<TextView>(){{add(tvSoup); add(tvKids); add(tvMids); add(tvZeytin); add(tvDurum); add(tvKebap); add(tvIzgara); add(tvPide); add(tvTatli); add(tvSalata); add(tvDrinks); add(tvMangal);}};

        llLang = (LinearLayout)findViewById(R.id.llLang);

        llLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLang();
            }
        });

        ivFlag = (ImageView)findViewById(R.id.ivFlag);

        //SetViews
        ibSoup = (LinearLayout)findViewById(R.id.ibSoup);
        ibKids = (LinearLayout)findViewById(R.id.ibKids);
        ibMids = (LinearLayout)findViewById(R.id.ibMids);
        ibZeytin = (LinearLayout)findViewById(R.id.ibZeytin);
        ibDurum = (LinearLayout)findViewById(R.id.ibDurum);
        ibKebap = (LinearLayout)findViewById(R.id.ibKebap);
        ibIzgara = (LinearLayout)findViewById(R.id.ibIzgara);
        ibPide = (LinearLayout)findViewById(R.id.ibPide);
        ibTatli = (LinearLayout)findViewById(R.id.ibTatli);
        ibSalata = (LinearLayout)findViewById(R.id.ibSalata);
        ibDrinks = (LinearLayout)findViewById(R.id.ibDrinks);
        ibMangal = (LinearLayout)findViewById(R.id.ibMangal);

        ibArray = new ArrayList<LinearLayout>(){{add(ibSoup); add(ibKids); add(ibMids); add(ibZeytin); add(ibDurum); add(ibKebap); add(ibIzgara); add(ibPide); add(ibTatli); add(ibSalata); add(ibDrinks); add(ibMangal);}};
        for(int i = 0; i<ibArray.size();i++){
            LinearLayout ib = ibArray.get(i);
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Clicked", ""+v.getId());
                    Intent in = new Intent(CategoryActivity.this, MenuActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("lang", currLang);
                    bundle.putInt("cat", ibArray.indexOf((LinearLayout)v));
                    in.putExtras(bundle);
                    if(firstTime)
                        startActivity(in);
                    else{
                        Intent retIn = new Intent();
                        retIn.putExtras(bundle);
                        setResult(RESULT_OK, retIn);
                        finish();
                    }
                }
            });
        }

        btnLang = (Button)findViewById(R.id.btnLang);
        btnLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLang();
            }
        });

        setStrings();

    }

    public void setLang(){
        AlertDialog.Builder  builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Language");
        ListView lvLangs = new ListView(this);
        lvLangs.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.lang_list)));
        builder.setView(lvLangs);
        final AlertDialog dialog = builder.create();
        lvLangs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currLang = position;
                setStrings();
                dialog.cancel();
            }
        });


        builder.show();

    }

    public void setStrings(){
        tvLang.setText(getResources().getStringArray(R.array.lang_list)[currLang]);

        ivFlag.setImageDrawable(getResources().getDrawable(new int[]{R.drawable.flag_turkey, R.drawable.flag_united_kingdom, R.drawable.flag_united_arab_emirates}[currLang]));

        for(int i = 0; i < tvArray.size();i++) {
            tvArray.get(i).setText(DataHandler.getReadyCategories().get(i).getName(currLang));
        }
    }
}
