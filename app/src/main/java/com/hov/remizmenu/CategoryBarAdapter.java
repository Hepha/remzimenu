package com.hov.remizmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hepha on 12.10.2016.
 */
public class CategoryBarAdapter extends ArrayAdapter<Category> {

    private int currLang = 0;

    public CategoryBarAdapter(Context context, int resource, int currLang) {
        super(context, resource);
        this.currLang = currLang;
    }

    public CategoryBarAdapter(Context context, int resource, List<Category> objects, int currLang) {
        super(context, resource, objects);
        this.currLang = currLang;
    }

    public void setLang(int i){
        currLang = i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null){
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.adapter_category_bar, null);
        }

        Category c = getItem(position);

        TextView tvCat = (TextView)v.findViewById(R.id.tvCat);

        if(tvCat != null){
            tvCat.setText(c.getName(currLang));
        }

        return v;
    }


}
