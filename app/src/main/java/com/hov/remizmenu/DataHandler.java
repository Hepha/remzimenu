package com.hov.remizmenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hepha on 24.09.2016.
 */
public class DataHandler {
    public static List<Category> getReadyCategories(){
        List<Category> list = new ArrayList<>();

        list.add(new Category(new String[]{"Çorbalar", "Soups"}, R.drawable.kebap_tile, null));
        list.add(new Category(new String[]{"Çocuk Menu", "Kids Menu"}, R.drawable.kebap_tile, null));
        list.add(new Category(new String[]{"Ara Sıcaklar", "Entree"}, R.drawable.kebap_tile, null));
        list.add(new Category(new String[]{"Zeytinyağlılar", "Zeytinyağlılar"}, R.drawable.zeytins_tile, null));
        list.add(new Category(new String[]{"Dürümler", "Roll"}, R.drawable.zeytins_tile, null));
        list.add(new Category(new String[]{"Kebaplar", "Kebabs"}, R.drawable.kebap_tile, null));
        list.add(new Category(new String[]{"Izgaralar", "Grilled"}, R.drawable.izgara_tile, null));
        list.add(new Category(new String[]{"Pide ve Lahmacun", "Pita and Lahmacun"}, R.drawable.zeytins_tile, null));
        list.add(new Category(new String[]{"Tatlılar", "Desserts"}, R.drawable.zeytins_tile, null));
        list.add(new Category(new String[]{"Salatalar", "Salads"}, R.drawable.zeytins_tile, null));
        list.add(new Category(new String[]{"İçecekler", "Drinks"}, R.drawable.zeytins_tile, null));
        list.add(new Category(new String[]{"Mangal Başı", "Barbecue Upstairs"}, R.drawable.zeytins_tile, null));

        return list;
    }

    //THESE WILL BE MOVED TO EITHER A JSON OR CALLS FROM API

    /*
    0-Soup
    1-Mids
    2-Zeytin
    3-Durum
    4-Kebap
    5-Izgara
    6-Pide
    7-Tatli
    8-Salata
    9-Drinks
    10-Mangal
    */

    public static List<Item> getItemList(int id){
        switch (id){
            case 0:
                List<Item> soups = new ArrayList<>();
                soups.add(new Item(new String[]{"Günün Çorbası", "Soup of the Day"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 6.00));
                return soups;
            case 1:
                List<Item> kids = new ArrayList<>();
                kids.add(new Item(new String[]{"Çocuk Menu", "Kids Menu"}, new String[]{"Köfte veya Tavuk - Patates Tava", "Meatballs or Chicken - French Fries"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 15.00));
                return kids;
            case 2:
                List<Item> mids = new ArrayList<>();
                mids.add(new Item(new String[]{"İçli Köfte", "Stuffed Meatballs"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 6.00));
                mids.add(new Item(new String[]{"Fındık Lahmacun", "Fındık Lahmacun"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 3.00));
                mids.add(new Item(new String[]{"Patates Tava", "French Fries"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 8.00));
                mids.add(new Item(new String[]{"Kiremitte Mantar", "Kiremitte Mantar"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 12.00));
                return mids;
            case 3:
                List<Item> zeytins = new ArrayList<>();
                zeytins.add(new Item(new String[]{"Patlıcan Salata", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,9.00));
                zeytins.add(new Item(new String[]{"Patlıcan Soslu", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,9.00));
                zeytins.add(new Item(new String[]{"Yoğurtlu Patlıcan", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                zeytins.add(new Item(new String[]{"Acılı Ezme", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                zeytins.add(new Item(new String[]{"Al Biber", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                zeytins.add(new Item(new String[]{"Haydari", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,9.00));
                zeytins.add(new Item(new String[]{"Yaprak Sarma", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                zeytins.add(new Item(new String[]{"Taze Fasulye", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                zeytins.add(new Item(new String[]{"Ordövr Tabağı", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 17.00));
                return zeytins;
            case 4:
                List<Item> durums = new ArrayList<>();
                durums.add(new Item(new String[]{"Acılı Dürüm", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 16.00));
                durums.add(new Item(new String[]{"Acısız Dürüm", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 16.00));
                durums.add(new Item(new String[]{"Çöp Şiş Dürüm", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 19.00));
                durums.add(new Item(new String[]{"Kuzu Şiş Dürüm", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 19.00));
                durums.add(new Item(new String[]{"Piliç Şiş Dürüm", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 16.00));
                return durums;
            case 5:
                List<Item> kebaps = new ArrayList<>();
                kebaps.add(new Item(new String[]{"Etten İskender Kebap", "Etten İskender Kebab"}, new String[]{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vel sodales ex. Praesent in vestibulum dui. Pellentesque purus enim, pharetra laoreet nisl at, hendrerit elementum nunc. Integer in rutrum leo. Pellentesque ut lectus finibus, pulvinar dolor accumsan, convallis quam. Sed ut erat sapien. Quisque pellentesque lacinia arcu, in pharetra orci pharetra non. Donec pharetra maximus mauris eu congue. Nunc non lacus in tortor laoreet tincidunt. Duis vitae consequat ipsum. Quisque pretium mauris in tellus laoreet, vel faucibus leo facilisis. Donec a magna fringilla arcu finibus gravida. Donec vel ligula sollicitudin, sagittis tortor eget, consequat magna. Suspendisse a nibh sem.", "For later"}, new String[]{"30 Dakika", "30 Minutes"}, R.drawable.kebaps_iskender_tile,30.00));
                kebaps.add(new Item(new String[]{"Acılı Kebap", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_acili_tile,22.00));
                kebaps.add(new Item(new String[]{"Acısız Kebap", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_acisiz_tile,22.00));
                kebaps.add(new Item(new String[]{"Patlıcanlı Kebap", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_patlicanli_tile,27.00));
                kebaps.add(new Item(new String[]{"Domatesli Kebap", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_domatesli_tile,24.00));
                kebaps.add(new Item(new String[]{"Fıstıklı Kebap", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_fistikli_tile, 26.00));
                kebaps.add(new Item(new String[]{"Sarma Fırın Beyti", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_beyti_tile, 27.00));
                kebaps.add(new Item(new String[]{"Alinazik", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_alinazik_tile, 29.00));
                kebaps.add(new Item(new String[]{"Yoğurtlu Kebap", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaos_yogurtlu_tile, 26.00));
                return kebaps;
            case 6:
                List<Item> izgaras = new ArrayList<>();

                izgaras.add(new Item(new String[]{"Karışık Izgara", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,44.00));
                izgaras.add(new Item(new String[]{"Karışık Köfte", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,27.00));
                izgaras.add(new Item(new String[]{"Acılı Köfte", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,21.00));
                izgaras.add(new Item(new String[]{"Izgara Köfte", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 21.00));
                izgaras.add(new Item(new String[]{"Akçaabat Köfte", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 21.00));
                izgaras.add(new Item(new String[]{"Kuzu Pirzola", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,38.00));
                izgaras.add(new Item(new String[]{"Kuzu Şiş", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,28.00));
                izgaras.add(new Item(new String[]{"Çöp Şiş", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 28.00));
                izgaras.add(new Item(new String[]{"Kuzu Tandır", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile,24.00));
                izgaras.add(new Item(new String[]{"Kuzu Külbastı", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 29.00));
                izgaras.add(new Item(new String[]{"Et Beyti", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 32.00));
                izgaras.add(new Item(new String[]{"Antrikot", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 27.00));
                izgaras.add(new Item(new String[]{"Piliç Şiş", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 21.00));
                izgaras.add(new Item(new String[]{"Piliç Pirzola", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 21.00));
                izgaras.add(new Item(new String[]{"Piliç Kanat", "Remzi Kebab"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 21.00));
                return izgaras;
            case 7:
                List<Item> pides = new ArrayList<>();
                pides.add(new Item(new String[]{"Lahmacun", "Lahmacun"}, new String[]{"Sonra", "Very thin Turkish pizza covered with seasoned minced meat and onions"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 6.00));
                pides.add(new Item(new String[]{"Kavurmalı Pide", "Kavurmalı Pide"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 18.50));
                pides.add(new Item(new String[]{"Kıymalı Pide", "Kıymalı Pide"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 13.50));
                pides.add(new Item(new String[]{"Kaşarlı Pide", "Kaşarlı Pide"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 12.50));
                pides.add(new Item(new String[]{"Karışık Pide", "Karışık Pide"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 16.50));
                pides.add(new Item(new String[]{"Kuşbaşılı Pide", "Kuşbaşılı Pide"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 17.50));
                return pides;
            case 8:
                List<Item> tatlis = new ArrayList<>();
                tatlis.add(new Item(new String[]{"Kemal Paşa", "Kemap Paşa"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 8.00));
                tatlis.add(new Item(new String[]{"Fırın Sütlaç", "Fırın Sütlaç"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 8.00));
                tatlis.add(new Item(new String[]{"İncir Tatlısı", "İncir Tatlısı"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 10.00));
                tatlis.add(new Item(new String[]{"Ayva Tatlısı", "Mevisiminde Ayva Tatlısı"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                tatlis.add(new Item(new String[]{"Kabak Tatlısı", "Mevisiminde Kabak Tatlısı"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                tatlis.add(new Item(new String[]{"Künefe", "Künefe"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 12.00));
                return tatlis;
            case 9:
                List<Item> salatas = new ArrayList<>();
                salatas.add(new Item(new String[]{"Çoban Salata", "Çoban Salata"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.50));
                salatas.add(new Item(new String[]{"Mevsim Salata", "Mevsim Salata"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.50));
                salatas.add(new Item(new String[]{"Gavurdağ Salata", "Gavurdağ Salata"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 12.00));
                salatas.add(new Item(new String[]{"Yoğurt", "Yoğurt"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 8.00));
                salatas.add(new Item(new String[]{"Cacık", "Cacık"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 9.00));
                return salatas;
            case 10:
                List<Item> drinks = new ArrayList<>();
                drinks.add(new Item(new String[]{"Coca Cola", "Coca Cola"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 4.50));
                drinks.add(new Item(new String[]{"Fanta", "Fanta"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 4.50));
                drinks.add(new Item(new String[]{"Sprite", "Sprite"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 4.50));
                drinks.add(new Item(new String[]{"Meyve Suyu", "Meyve Suyu"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 4.50));
                drinks.add(new Item(new String[]{"Ayran", "Ayran"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 3.00));
                drinks.add(new Item(new String[]{"Soda", "Soda"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 2.00));
                drinks.add(new Item(new String[]{"Meyveli Soda", "Meyveli Soda"}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 2.50));
                return drinks;
            case 11:
                List<Item> mangal = new ArrayList<>();
                mangal.add(new Item(new String[]{"Kuzu Pirzola Kg.", "Kuzu Pirzola Kg."}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 142.00));
                mangal.add(new Item(new String[]{"Et Kg.", "Et Kg."}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 122.00));
                mangal.add(new Item(new String[]{"Köfte Kg.", "Köfte Kg."}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 92.00));
                mangal.add(new Item(new String[]{"Piliç Kg.", "Piliç Kg."}, new String[]{"Sonra", "For later"}, new String[]{"Ayrıca sonra", "Also later"}, R.drawable.kebaps_iskender_tile, 78.00));
                return mangal;
        }

        return null;
    }
}
