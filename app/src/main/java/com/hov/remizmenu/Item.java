package com.hov.remizmenu;


public class Item {
    private String[] name, details, cookTime;
    private int bitmapId;
    private double price;

    public Item(String[] name, String[] details, String[] cookTime, int bitmapId, double price) {
        this.name = name;
        this.details = details;
        this.cookTime = cookTime;
        this.bitmapId = bitmapId;
        this.price = price;
    }

    public Item(String[] name, String[] details, String[] cookTime, double price) {
        this.name = name;
        this.details = details;
        this.cookTime = cookTime;
        this.price = price;
    }

    public String[] getName(){return name;}
    public String[] getDetail(){return details;}
    public String[] getCookTime(){return cookTime;}

    public String getName(int i) {
        return name[i];
    }

    public void setName(String[] name) {
        this.name = name;
    }

    public String getDetails(int i) {
        return details[i];
    }

    public void setDetails(String[] details) {
        this.details = details;
    }

    public String getCookTime(int i) {
        return cookTime[i];
    }

    public void setCookTime(String[] cookTime) {
        this.cookTime = cookTime;
    }

    public int getBitmapId() {
        return bitmapId;
    }

    public void setBitmapId(int bitmapId) {
        this.bitmapId = bitmapId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
