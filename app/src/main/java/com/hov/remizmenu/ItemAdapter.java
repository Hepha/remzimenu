package com.hov.remizmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hepha on 28.09.2016.
 */
public class ItemAdapter extends ArrayAdapter<Item> {

    private onItemInteraction mListener;
    private int currLang = 0;

    public ItemAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ItemAdapter(Context context, int resource, Item[] objects) {
        super(context, resource, objects);
    }

    public ItemAdapter(Context context, int resource, List<Item> objects) {
        super(context, resource, objects);
        if (context instanceof onItemInteraction) {
            mListener = (onItemInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void setLang(int i){
        currLang = i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.adapter_item, null);
        }

        ImageView ivItem = (ImageView)v.findViewById(R.id.ivItem);
        TextView tvName = (TextView)v.findViewById(R.id.tvName);
        TextView tvPrice = (TextView)v.findViewById(R.id.tvPrice);
        TextView tvDetails = (TextView)v.findViewById(R.id.tvDetails);
        TextView tvCook = (TextView)v.findViewById(R.id.tvCook);
        Button btnAdd = (Button)v.findViewById(R.id.btnAdd);
        LinearLayout llItem = (LinearLayout)v.findViewById(R.id.llItem);

        Item i = getItem(position);

        if(ivItem !=null){
            ivItem.setImageDrawable(getContext().getResources().getDrawable(i.getBitmapId()));
        }
        if(tvName !=null){
            tvName.setText(i.getName(currLang));
        }
        if(tvPrice != null){
            tvPrice.setText(String.format("%.2f", i.getPrice()) + "TL");
        }
        if(tvDetails !=null){
            tvDetails.setText(i.getDetails(currLang));
        }
        if(tvCook !=null){
            tvCook.setText(i.getCookTime(currLang));
        }
        if(btnAdd !=null){
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.addItem(getItem(position));
                }
            });
        }
        if(llItem!=null){
            llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.showDialog(getItem(position));
                }
            });
        }

        return v;
    }

    public interface onItemInteraction{
        void addItem(Item i);
        void showDialog(Item i);
    }
}
