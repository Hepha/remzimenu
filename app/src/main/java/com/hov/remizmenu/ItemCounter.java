package com.hov.remizmenu;

/**
 * Created by Hepha on 13.10.2016.
 */
public class ItemCounter extends Item{

    private int counter = 0;

    public ItemCounter(String[] name, String[] details, String[] cookTime, int bitmapId, double price, int counter) {
        super(name, details, cookTime, bitmapId, price);
        this.counter = counter;
    }

    public ItemCounter(Item item) {
        super(item.getName(), item.getDetail(), item.getCookTime(), item.getBitmapId(), item.getPrice());
        this.counter = 1;
    }

    public void counterUp(){
        counter++;
    }

    public void counterDown(){
        counter--;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
