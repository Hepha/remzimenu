package com.hov.remizmenu;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MenuActivity extends AppCompatActivity implements ItemAdapter.onItemInteraction, AddedItemAdapter.onRemoveInteraction {

    //VIEWS
    ListView lvItems, lvAdded, lvCategories;
    TextView tvTotal, tvLang, tvTotalAmount, tvCategories, tvDessert, tvDrinks, tvMids, tvSalads, tvSort, tvNote1, tvNote2;
    RadioButton rbAlp, rbPrice;
    LinearLayout llLang;
    LinearLayout ibCat, ibDessert, ibDrinks, ibMids, ibSalads;

    Button btnClear;

    ImageView ivFlag;

    //Lists
    List<Item> itemList;//, addedItemList;
    List<ItemCounter> addedItemList;

    //Adapter
    ItemAdapter itemAdapter;
    AddedItemAdapter addedItemAdapter;
    CategoryBarAdapter categoryBarAdapter;

    //Values
    int currLang = 0; //0-Turkish 1-English 2-Arabic
    int currCategory = 0;
    boolean isAlp = false, isPrice = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_menu);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            currLang = bundle.getInt("lang");
            currCategory = bundle.getInt("cat");
        }

        lvItems = (ListView) findViewById(R.id.lvItems);
        itemList = DataHandler.getItemList(currCategory);
        if (itemList.isEmpty()) {
            Toast.makeText(this, "There is nothing in this category!", Toast.LENGTH_SHORT).show();
        }
        itemAdapter = new ItemAdapter(this, R.layout.adapter_item, itemList);
        lvItems.setAdapter(itemAdapter);

        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showItemDialog(itemList.get(position));
            }
        });


        lvAdded = (ListView) findViewById(R.id.lvAdded);
        addedItemList = new ArrayList<ItemCounter>();
        addedItemAdapter = new AddedItemAdapter(this, R.layout.adapter_added_item, addedItemList);
        lvAdded.setAdapter(addedItemAdapter);

        lvCategories = (ListView) findViewById(R.id.lvCats);
        categoryBarAdapter = new CategoryBarAdapter(this, R.layout.adapter_category_bar, DataHandler.getReadyCategories(), currLang);
        lvCategories.setAdapter(categoryBarAdapter);
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currCategory = position;
                setList();
            }
        });

        //Button
        btnClear = (Button)findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addedItemList.clear();
                addedItemAdapter.notifyDataSetChanged();
                setTotal();
                tvNote1.setVisibility(View.VISIBLE);
                btnClear.setVisibility(View.GONE);
            }
        });


        //TextViews
        tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
        tvCategories = (TextView) findViewById(R.id.tvCategories);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvLang = (TextView) findViewById(R.id.tvLang);
        tvDessert = (TextView) findViewById(R.id.tvDessert);
        tvDrinks = (TextView) findViewById(R.id.tvDrinks);
        tvMids = (TextView) findViewById(R.id.tvMids);
        tvSalads = (TextView) findViewById(R.id.tvSalads);
        tvSort = (TextView) findViewById(R.id.tvSort);
        tvNote1 = (TextView) findViewById(R.id.tvNote1);
        tvNote2 = (TextView) findViewById(R.id.tvNote2);


        rbAlp = (RadioButton) findViewById(R.id.rbAlp);
        rbPrice = (RadioButton) findViewById(R.id.rbPrice);

        rbAlp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isAlp = isChecked;
                if (isChecked) {
                    Log.d("Checked", "Alphabet");
                    sortAlphabetically();
                }
            }
        });

        rbPrice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPrice = isChecked;
                if (isChecked) {
                    Log.d("Checked", "Price");
                    sortByPrice();
                }
            }
        });


        llLang = (LinearLayout) findViewById(R.id.llLang);
        llLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLang();
            }
        });

        ivFlag = (ImageView) findViewById(R.id.ivFlag);


        ibCat = (LinearLayout) findViewById(R.id.llCategories);
        ibCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCats();
            }
        });

        ibDessert = (LinearLayout) findViewById(R.id.llDesert);
        ibDessert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currCategory = 8;
                setList();
            }
        });

        ibDrinks = (LinearLayout) findViewById(R.id.llDrinks);
        ibDrinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currCategory = 10;
                setList();
            }
        });

        ibMids = (LinearLayout) findViewById(R.id.llMids);
        ibMids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currCategory = 2;
                setList();
            }
        });

        ibSalads = (LinearLayout) findViewById(R.id.llSalads);
        ibSalads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currCategory = 9;
                setList();
            }
        });


        setTotal();
        setStrings();

    }

    public void openCats() {
        Intent i = new Intent(MenuActivity.this, CategoryActivity.class);
        Bundle b = new Bundle();
        b.putInt("lang", currLang);
        i.putExtras(b);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();

                if (bundle != null) {
                    currLang = bundle.getInt("lang");
                    currCategory = bundle.getInt("cat");
                }

                setStrings();
                setList();

                addedItemAdapter.setlang(currLang);
                addedItemAdapter.notifyDataSetChanged();

                categoryBarAdapter.setLang(currLang);
                categoryBarAdapter.notifyDataSetChanged();
            }
        }

    }

    public void setList() {
        itemList.clear();
        itemList.addAll(DataHandler.getItemList(currCategory));
        itemAdapter.setLang(currLang);

        if (isAlp)
            sortAlphabetically();
        else if (isPrice)
            sortByPrice();
        else
            itemAdapter.notifyDataSetChanged();

        if (itemList.isEmpty()) {
            Toast.makeText(this, "There is nothing in this category!", Toast.LENGTH_SHORT).show();
        }
    }

    public void setStrings() {
        //TextViews
        tvCategories.setText(getResources().getStringArray(R.array.str_category)[currLang]);
        tvTotalAmount.setText(getResources().getStringArray(R.array.str_total_amunt)[currLang] + ": ");
        tvLang.setText(getResources().getStringArray(R.array.lang_list)[currLang]);
        tvDessert.setText(getResources().getStringArray(R.array.str_desserts)[currLang]);
        tvDrinks.setText(getResources().getStringArray(R.array.str_drinks)[currLang]);
        tvMids.setText(getResources().getStringArray(R.array.str_mids)[currLang]);
        tvSalads.setText(getResources().getStringArray(R.array.str_salads)[currLang]);
        tvSort.setText(getResources().getStringArray(R.array.str_sort)[currLang] + ": ");
        rbAlp.setText(getResources().getStringArray(R.array.str_alp)[currLang]);
        rbPrice.setText(getResources().getStringArray(R.array.str_by_price)[currLang]);
        tvNote1.setText(getResources().getStringArray(R.array.str_note_1)[currLang]);
        tvNote2.setText(getResources().getStringArray(R.array.str_note_2)[currLang]);
        btnClear.setText(getResources().getStringArray(R.array.str_clear_all)[currLang]);


        //Flag
        ivFlag.setImageDrawable(getResources().getDrawable(new int[]{R.drawable.flag_turkey, R.drawable.flag_united_kingdom, R.drawable.flag_united_arab_emirates}[currLang]));

    }


    public void setLang() {
        AlertDialog.Builder  builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Language");
        ListView lvLangs = new ListView(this);
        lvLangs.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.lang_list)));
        builder.setView(lvLangs);
        final AlertDialog dialog = builder.create();
        lvLangs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currLang = position;
                setStrings();

                itemAdapter.setLang(currLang);
                if (isAlp)
                    sortAlphabetically();
                else if (isPrice)
                    sortByPrice();
                else
                    itemAdapter.notifyDataSetChanged();

                addedItemAdapter.setlang(currLang);
                addedItemAdapter.notifyDataSetChanged();

                categoryBarAdapter.setLang(currLang);
                categoryBarAdapter.notifyDataSetChanged();

                dialog.cancel();
            }
        });



        dialog.show();

    }

    public void showItemDialog(final Item item) {
        AlertDialog.Builder  builder = new AlertDialog.Builder(this);
        builder.setTitle(item.getName(currLang));
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_item, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        ImageView ivItem = (ImageView) view.findViewById(R.id.ivItem);
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
        TextView tvCook = (TextView) view.findViewById(R.id.tvCook);
        TextView tvDetails = (TextView) view.findViewById(R.id.tvDetails);
        Button btnBack = (Button) view.findViewById(R.id.btnBack);
        Button btnAdd = (Button) view.findViewById(R.id.btnAdd);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem(item);
                dialog.cancel();
            }
        });

        ivItem.setImageDrawable(getResources().getDrawable(item.getBitmapId()));
        tvName.setText(item.getName(currLang));
        tvPrice.setText(getResources().getStringArray(R.array.str_price)[currLang] + ": " + String.format("%.2f", item.getPrice()) + "TL");
        tvCook.setText(getResources().getStringArray(R.array.str_cooking_duration)[currLang] + ": " + item.getCookTime(currLang));
        tvDetails.setText(item.getDetails(currLang));
        btnBack.setText(getResources().getStringArray(R.array.str_go_back)[currLang]);

        dialog.show();
    }

    public void setTotal() {
        double total = 0;
        for (ItemCounter i : addedItemList) {
            total += i.getPrice() * i.getCounter();
        }
        tvTotal.setText(String.format("%.2f", total) + " TL");
    }

    public void sortAlphabetically() {
        Collections.sort(itemList, new Comparator<Item>() {
            @Override
            public int compare(Item lhs, Item rhs) {
                return lhs.getName(currLang).compareTo(rhs.getName(currLang));
            }
        });

        itemAdapter.notifyDataSetChanged();
    }

    public void sortByPrice() {
        Collections.sort(itemList, new Comparator<Item>() {
            @Override
            public int compare(Item lhs, Item rhs) {
                return Double.compare(lhs.getPrice(), rhs.getPrice());
            }
        });

        itemAdapter.notifyDataSetChanged();
    }

    @Override
    public void addItem(Item i) {
        tvNote1.setVisibility(View.GONE);
        btnClear.setVisibility(View.VISIBLE);
        boolean check = false;
        for (int j = 0; j < addedItemList.size(); j++) {
            if (addedItemList.get(j).getName(currLang) == i.getName(currLang)) {
                check = true;
                addedItemList.get(j).counterUp();
                break;
            }
        }
        if (!check)
            addedItemList.add(new ItemCounter(i));
        addedItemAdapter.notifyDataSetChanged();
        lvAdded.setSelection(addedItemAdapter.getCount() - 1);
        setTotal();
    }

    @Override
    public void showDialog(Item i) {
        showItemDialog(i);
    }

    @Override
    public void removeItem(int position) {
        boolean check = false;
        for (int j = 0; j < addedItemList.size(); j++) {
            if (addedItemList.get(j).getName(currLang) == addedItemList.get(position).getName(currLang)) {
                check = true;
                addedItemList.get(j).counterDown();
                if (addedItemList.get(j).getCounter() == 0)
                    check = false;
                break;
            }
        }
        if (!check)
            addedItemList.remove(position);
        addedItemAdapter.notifyDataSetChanged();
        setTotal();

        if(addedItemList.size() == 0) {
            tvNote1.setVisibility(View.VISIBLE);
            btnClear.setVisibility(View.GONE);
        }
    }
    @Override
    public void clearItem(int position){
        addedItemList.remove(position);
        addedItemAdapter.notifyDataSetChanged();

        //Put these in a different view update function
        setTotal();

        if(addedItemList.size() == 0) {
            tvNote1.setVisibility(View.VISIBLE);
            btnClear.setVisibility(View.GONE);
        }
    }


    @Override
    public void openItem(int position) {
        showItemDialog(addedItemList.get(position));
    }

    @Override
    public void onBackPressed() {
        openCats();
    }
}
